import re
import time
import praw
import inspect
import logging
import sqlite3
import requests
import OAuth2Util

# configure the logger
logging.basicConfig(filename='ctrl-f_bot.log',
                    level=logging.INFO,
                    format='%(asctime)s %(message)s')

# User config
# --------------------------------------------------------------------
# Don't include the /r/
SUBREDDIT_NAME = 'all'

KEYWORDS = ['source', 'original', 'video', 'youtube link', 'sauce',
            'liveleak', 'vimeo', 'article']

# the username to foward the error messages to
OWNER = ''

REPLY = '''
{}

*{}^(^([s](http://www.icndb.com/)))*
-------

^(This is an automatic message to help Redditors using the CTRL+F function 
find the source of the post.
If I am wrong
[send me a message](https://www.reddit.com/message/compose?to=CTRL-F_Bot&subject=you%20got%20it%20wrong&message={}).
^(I'm new and looking to improve.) 
If you wish for me to never reply to you ever again
[click this](https://www.reddit.com/message/compose?to=CTRL-F_Bot&subject=dont%20bother%20me).
Mods if you want me gone from your sub
[click this](https://www.reddit.com/message/compose?to=CTRL+F_Bot&subject=get%20out%of%20{}).)
'''

# --------------------------------------------------------------------


# try to import my bot config
try:
    import bot
    OWNER = bot.OWNER
except ImportError:
    pass


def top_level_comment(r, cmt):
    # get the parent, this will return a submission object if it's a
    # top level comment
    parent = r.get_info(thing_id=cmt.parent_id)

    if type(parent) is praw.objects.Submission:
        return True
    else:
        return False


def source_comment(cmt):
    # regex to match urls to check that the comment has a link in it
    url_regex = r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)'
    # compile the regex pattern along with checking for any of our keywords in the comment
    pattern = re.compile(r'(?=.*({}))(?=.*{})'.format('|'.join(KEYWORDS), url_regex), re.I)

    # if it matches the pattern return true
    if pattern.match(str(cmt)):
        logging.info('''\n\tMatched comment:
        {}
        Link: {}
        re matches: {}
        '''.format(cmt, cmt.permalink, pattern.findall(str(cmt))))
        return True
    else:
        return False


def add_comment(cmt, sql, c):
    print('Replying to comment...')
    print(cmt.permalink)
    resp = requests.get('http://api.icndb.com/jokes/random')
    joke = resp.json()['value']['joke']
    print(joke)
    # cmt.reply(REPLY.format(', '.join(KEYWORDS), joke, cmt.permalink, cmt.subreddit))

    c.execute('INSERT INTO replied VALUES(?)', [cmt.id])
    sql.commit()


def add_to_dnr_list(author, sql, c):
    print('Adding user to do not respond list...')
    c.execute('INSERT INTO do_not_respond VALUES(?)', [author])
    sql.commit()


def forward_message(r, msg):
    print('Fowarding message...')
    r.send_message(OWNER, 'FW: ' + msg.subject, str(msg), captcha=None)


def check_if_user_is_mod(r, subname, user):
    # get the subreddit
    sub = r.get_subreddit(subname)
    try:  # try to get a list of the mods
        mods = list(sub.get_moderators())
    except praw.errors.InvalidSubreddit:  # catch it if its invalid
        # log the incident and move on
        logging.error('Invalid subreddit {}'.format(subname))
        return False

    for mod in mods:
        if user.lower() == mod.lower():
            return True

    return False


def add_sub_to_banned(r, msg, sql, c):
    sub = msg.subject[11:]  # get the sub from the subject
    user = str(msg.author)  # get the username of the author

    if check_if_user_is_mod(r, msg):
        print('Adding sub to banned from list...')
        c.execute('INSERT INTO banned_subs VALUES(?)', [sub])
        sql.commit()

        # then reply letting them know the bot won't post anymore
        msg.reply('I will no longer post in {}, I apologize for\
                   any inconvinience.')


def check_messages(r, c, sql):
    messages = r.get_unread()
    print('Checking messages...')
    # run a function if necessary then mark as read
    for msg in messages:
        if msg.subject == 'dont bother me':
            add_to_dnr_list(str(msg.author), sql, c)
        elif msg.subject == 'you got it wrong':
            forward_message(r, msg)
        elif 'get out of' in msg.subject:
            add_sub_to_banned(r, msg, sql, c)

        msg.mark_as_read()


def find_comments(r, c, sql):
    i = 0  # counter for sub tasks

    print('Scanning comments...')
    cmt_stream = praw.helpers.comment_stream(r, SUBREDDIT_NAME, limit=None)
    for cmt in cmt_stream:
        i += 1  # increment the counter
        # check if the comment is in a sub we are banned from
        c.execute('SELECT * FROM banned_subs WHERE sub = ?', [str(cmt.subreddit)])
        if c.fetchone():  # continue if we're banned from the sub
            continue

        # check if the comment is a top_level comment and a source comment
        if not top_level_comment(r, cmt) or not source_comment(cmt):
            continue

        # check if they are on the dnr list
        c.execute('SELECT * FROM do_not_respond WHERE username = ?', [str(cmt.author)])
        if c.fetchone():  # continue if they are on the dnr list
            continue

        # check if we've already replied to this one
        c.execute('SELECT * FROM replied WHERE comment_id = ?', [cmt.id])
        if c.fetchone():  # continue if we've already replied
            continue

        # if the comment passed all the above, respond to it
        add_comment(cmt, sql, c)

        # every 100 comments do sub tasks
        if not i % 100:
            check_messages(r, c, sql)


def format_var_str(dic):
    s = ''
    for var, val in dic.items():
        s += ('\t' * 8) + '{}: {}\n'.format(var, val)

    return s


def main():
    r = praw.Reddit(user_agent='comment ctrl-f for source v1.5 /u/cutety')
    o = OAuth2Util.OAuth2Util(r, print_log=True)
    o.refresh(force=True)  # force the token to automatically refresh

    print('Loading database...')
    # Setup/Load SQL database
    sql = sqlite3.connect('ctrl-f_bot.db')
    c = sql.cursor()

    # Setup schema
    c.execute('CREATE TABLE IF NOT EXISTS replied(comment_id TEXT)')
    c.execute('CREATE TABLE IF NOT EXISTS do_not_respond(username TEXT)')
    c.execute('CREATE TABLE IF NOT EXISTS banned_subs(sub TEXT)')
    sql.commit()  # save the changes

    print('Database loaded.')

    # Main loop
    while True:
        try:
            find_comments(r, c, sql)
        except Exception as e:
            print('ERROR: {}'.format(e))

            # log the error event with specific information from the traceback
            traceback_log = '''
            ERROR: {e}
            File "{fname}", line {lineno}, in {fn}
            Time of error: {t}

            Variable dump:

            {g_vars}
            {l_vars}
            '''
            # grabs the traceback info
            frame, fname, lineno, fn = inspect.trace()[-1][:-2]
            # dump the variables and get formated strings
            g_vars = 'Globals:\n' + format_var_str(frame.f_globals)
            l_vars = 'Locals:\n' + format_var_str(frame.f_locals)

            # finally log the error
            logging.error(traceback_log.format(e=e, lineno=lineno, fn=fn,
                                               fname=fname, t=time.strftime('%c'),
                                               g_vars=g_vars, l_vars=l_vars))

        print('Sleeping...')
        time.sleep(300)  # go to sleep for a little bit


if __name__ == '__main__':
    if not SUBREDDIT_NAME:
        print('Subreddit name has not been set.\nExiting...')
        exit(1)

    main()